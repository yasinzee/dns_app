import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {UpdateDetailPage} from './update-detail-page';

@NgModule({
  declarations: [
    UpdateDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateDetailPage),
  ],
  exports: [
    UpdateDetailPage
  ]
})
export class UpdateDetailPageModule {}