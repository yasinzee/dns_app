import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';

@IonicPage()
@Component({
  selector: 'order-detail-page',
  templateUrl: 'order-detail-page.html'
})
export class OrderDetailPage {
  private productList: any = [];
  private freebieProductList: any = [];
  private totalPrice: any;
  private buyer: any;
  private paidAmount: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public extraComponents: ExtraComponents,
    public alerCtrl: AlertController,
    public http: Http) {

    menuCtrl.enable(true);
    this.navCtrl.swipeBackEnabled = false;
    this.totalPrice = this.navParams.get("totalPrice");
    this.productList = this.navParams.get("productList");
    this.freebieProductList = this.navParams.get("freebieProductList");
    this.buyer = this.navParams.get("buyer");
    console.log("Buyer: " + this.buyer);

    console.log(this.navParams.get("productList"));
    console.log("FreeBie Products");
    console.log(this.navParams.get("freebieProductList"));
  }

  submit() {
    let orderDetails = {
      "productList": this.productList,
      "freebieProductList": this.freebieProductList,
      "totalPrice": this.totalPrice,
      "consumerId": this.buyer,
      "distributorId": JSON.parse(localStorage.getItem('distributor')).id,
      "amountPaid": this.paidAmount,
      "amountDues": (this.totalPrice-this.paidAmount)
    };

    console.log(this.paidAmount);
    console.log(orderDetails);

    let confirm = this.alerCtrl.create({
      title: 'Order Confirm?',
      message: " Subtotal: "+this.totalPrice + "<br>Paid Amount: " + this.paidAmount + "<br>Dues Amount: "+ (this.totalPrice-this.paidAmount),
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {

            this.saveOrders(orderDetails).toPromise()
              .then(result => {
                console.log("Order saved successfully");
                console.log(result.json());
                this.navCtrl.push('OrderSuccessPage');
              }).catch(error => {
                this.extraComponents.showAlert("Something went wrong. Plz try again!!", error);
                console.log("Error while Saving Order" + error);
              });
          }
        }
      ]
    });
    confirm.present();
  }

  saveOrders(orderDetails: any) {
    let headers = new Headers();
    headers.append("Accept", "application/json");
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    console.log("url: " + AppConfig.cfg.apiUrl + AppConfig.cfg.saveOrder);
    return this.http.post(AppConfig.cfg.apiUrl + AppConfig.cfg.saveOrder, orderDetails, options);
  }
}
