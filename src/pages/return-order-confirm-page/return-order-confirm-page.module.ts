import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ReturnOrderConfirmPage} from './return-order-confirm-page';

@NgModule({
  declarations: [
    ReturnOrderConfirmPage,
  ],
  imports: [
    IonicPageModule.forChild(ReturnOrderConfirmPage),
  ],
  exports: [
    ReturnOrderConfirmPage
  ]
})
export class ReturnOrderConfirmPageModule {}