import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams, AlertController } from 'ionic-angular';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';
import { HttpClient } from '../../providers/http-client';

@IonicPage()
@Component({
  selector: 'return-order-confirm-page',
  templateUrl: 'return-order-confirm-page.html'
})
export class ReturnOrderConfirmPage {
  private productList: any = [];
  private freebieProductList: any = [];
  private totalPrice: any;
  private buyer: any;
  private paidAmount: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public extraComponents: ExtraComponents,
    public alerCtrl: AlertController,
    public httpClient: HttpClient) {

    menuCtrl.enable(true);
    this.navCtrl.swipeBackEnabled = false;
    this.totalPrice = this.navParams.get("totalPrice");
    this.productList = this.navParams.get("productList");
    this.freebieProductList = this.navParams.get("freebieProductList");

    console.log(this.navParams.get("productList"));
    console.log("FreeBie Products");
    console.log(this.navParams.get("freebieProductList"));
  }

  submit() {
    let orderDetails = {
      "productList": this.productList,
      "freebieProductList": this.freebieProductList,
      "totalPrice": this.totalPrice,
      "distributorId": JSON.parse(localStorage.getItem('distributor')).id,
    };

    console.log(this.paidAmount);
    console.log(orderDetails);

    let confirm = this.alerCtrl.create({
      title: 'Confirm?',
      message: "Are you agree to return selected products?",
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {

            this.applyReturn(orderDetails).toPromise()
              .then(result => {
                console.log("Order Returned successfully");
                console.log(result.json());
                this.extraComponents.showToast("Return request processed successfully");
                this.navCtrl.setRoot('DistributorOrdersPage');
              }).catch(error => {
                this.extraComponents.showAlert("Something went wrong. Plz try again!!", error);
                console.log("Error while Saving Order" + error);
              });
          }
        }
      ]
    });
    confirm.present();
  }

  applyReturn(orderDetails: any) {
    let id = this.navParams.get('orderId');
    let param: any = '?id=' + id;
    
    return this.httpClient.post(AppConfig.cfg.applyReturn + param, orderDetails);
  }
}
