import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {RegisterDistributorPage} from './register-distributor-page';

@NgModule({
  declarations: [
    RegisterDistributorPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterDistributorPage),
  ],
  exports: [
    RegisterDistributorPage
  ]
})
export class RegisterDistributorPageModule {}