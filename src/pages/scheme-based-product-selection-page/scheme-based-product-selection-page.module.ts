import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SchemBasedProductSelectionPage} from './scheme-based-product-selection-page';

@NgModule({
  declarations: [
    SchemBasedProductSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(SchemBasedProductSelectionPage),
  ],
  exports: [
    SchemBasedProductSelectionPage
  ]
})
export class SchemBasedProductSelectionPageModule {}