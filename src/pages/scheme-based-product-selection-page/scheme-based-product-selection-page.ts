import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import *  as AppConfig from '../../app/config';

@IonicPage()
@Component({
    selector: 'scheme-based-product-selection-page',
    templateUrl: 'scheme-based-product-selection-page.html',
})

export class SchemBasedProductSelectionPage {
    private consumerData: FormGroup;
    private itemVal: string
    private totalPrice: any;
    private productList: any = [];
    private searchProductList: any = [];
    private amount:any = []; 
    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public http: Http) {
        
        this.productList = this.navParams.get('freebieProductList');
        this.totalPrice = 0;
    }
    
    ionViewDidLoad() {
    }

    subtract(product: any) {
        if(undefined == this.amount[product.id])
            this.amount[product.id] = 0;
        if ((product.count+product.count) > 0)
            product.count--;
        this.totalPrice -= product.totalPrice;
        product.totalPrice = this.amount[product.id] * product.count;
        this.totalPrice += product.totalPrice;
    }

    add(product: any) {
        if(undefined == this.amount[product.id])
            this.amount[product.id] = 0;
        if ((product.count+product.count) < (product.totalStock - product.soldStock))
            product.count++;
        this.totalPrice -= product.totalPrice;
        product.totalPrice = this.amount[product.id] * product.count;
        this.totalPrice += product.totalPrice;
        console.log(this.amount[product.id]+" val");
        
    }

    submit() {
        this.navCtrl.push('OrderDetailPage', { productList: this.navParams.get('productList'), freebieProductList: this.productList, totalPrice: this.totalPrice+this.navParams.get('totalPrice'), buyer: this.navParams.get('buyer') })
    }

    getItems(ev) {
        // set val to the value of the ev target
        var val = ev.target.value;

        console.log('product search');
        this.productList = this.searchProductList;
        this.productList = this.productList.filter((item) => {
            return (item.productId.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
    }
}

