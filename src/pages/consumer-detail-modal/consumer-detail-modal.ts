import { Component } from '@angular/core';
import { NavController, NavParams, ViewController} from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '../../providers/http-client';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';

@Component({
    templateUrl: 'consumer-detail-modal.html'
})
export class ConsumerDetailModal {
    private consumer:any;
    private consumerData: FormGroup;
    constructor(public navParams: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public formBuilder: FormBuilder,
        public extraComponents: ExtraComponents,
        public httpClient: HttpClient) {
        this.consumer = this.navParams.get('consumer');

        this.consumerData = this.formBuilder.group({
            name: [null],
            email: [null],
            mobile: [null],
            state: [null],
            city: [null],
            address: [null],
            duesAmount: [null]
        });
        this.initFields();
        this.fetchDuesAmount();
    }

    initFields(){
        this.consumerData.controls['name'].setValue(this.consumer.name);
        this.consumerData.controls['email'].setValue(this.consumer.email);
        this.consumerData.controls['mobile'].setValue(this.consumer.mobile);
        this.consumerData.controls['state'].setValue(this.consumer.state);
        this.consumerData.controls['city'].setValue(this.consumer.city);
        this.consumerData.controls['address'].setValue(this.consumer.address);
    }

    edit(){
        this.navCtrl.push('RegisterConsumerPage', {consumerData:this.consumer});
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    fetchDuesAmount(){
        let url = AppConfig.cfg.fetchConsumerTotalDuesAmount + "?id="+this.consumer.id;
        this.httpClient.get(url).toPromise()
        .then(result => {
          console.log(result.json());
          this.consumerData.controls['duesAmount'].setValue(result.json());
        }).catch(error => {
          this.extraComponents.showToast('Error occurd while fetching Dues amount');
        });
    }
}