import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';

@IonicPage()
@Component({
    selector: 'inventory-page',
    templateUrl: 'inventory.html',
})

export class InventoryPage {
    private productList: any = [];
    private searchProductList: any = [];
    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,
        public http: Http) {
    }

    ionViewDidLoad() {
        this.extraComponents.showLoading();
        this.getInventoryDetails().toPromise()
            .then(result => {
                this.extraComponents.closeLoading();
                console.log(result.json());
                this.productList = result.json();
                this.searchProductList = this.productList;
            }).catch(error => {
                this.extraComponents.closeLoading();
                console.log("Error while fetching inventory list" + error);
            });
    }

    getInventoryDetails() {
        console.log("params");
        let id = JSON.parse(localStorage.getItem('distributor')).id;

        let param: any = '?id=' + id;
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.inventoryDetails + param);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.inventoryDetails + param, options);
    }

    getItems(ev) {
        // set val to the value of the ev target
        var val = ev.target.value;

        console.log('product search');
        this.searchProductList = this.productList;
        this.searchProductList = this.productList.filter((item) => {
            return (item.productId.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });

    }
}

