
import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ConsumerListPage} from './consumer-list-page';

@NgModule({
  declarations: [
    ConsumerListPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsumerListPage),
  ],
  exports: [
    ConsumerListPage
  ]
})
export class ConsumerListPageModule {}