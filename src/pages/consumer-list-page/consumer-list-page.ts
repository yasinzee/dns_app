import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { ExtraComponents } from '../../providers/extra-components';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';

@IonicPage()
@Component({
    selector: 'consumer-list-page',
    templateUrl: 'consumer-list-page.html'
})
export class ConsumerListPage {
    private consumer:any;
    private consumerList: any = [];
    private searchConsumerList: any = [];
    
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents, 
        public http: Http) {

    }
    ionViewDidLoad() {
        this.getConsumerList().toPromise()
            .then(result => {
                console.log(result.json());
                this.consumerList = result.json();
                this.searchConsumerList = this.consumerList;
            }).catch(error => {
                console.log("Error while fetching inventory list" + error);
            });
    }

    getConsumerList() {
        let state = JSON.parse(localStorage.getItem('distributor')).state;
        let city = JSON.parse(localStorage.getItem('distributor')).city;

        let param: any = '?state=' + state + '&city=' + city;
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchConsumerList + param);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchConsumerList + param, options);
    }

    getItems(ev) {
        // set val to the value of the ev target
        var val = ev.target.value;

        console.log('product search');
        this.consumerList = this.searchConsumerList;
        this.consumerList = this.searchConsumerList.filter((item) => {
            return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
    }

    proceed(){
        console.log("proceed " + this.consumer);
        this.navCtrl.push('SellingProductPage', {consumerId:this.consumer});
    }

    addConsumer() {
        this.navCtrl.push('RegisterConsumerPage');
    }
}
