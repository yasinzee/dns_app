import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SellingProductPage} from './selling-product-page';

@NgModule({
  declarations: [
    SellingProductPage,
  ],
  imports: [
    IonicPageModule.forChild(SellingProductPage),
  ],
  exports: [
    SellingProductPage
  ]
})
export class SellingProductPageModule {}