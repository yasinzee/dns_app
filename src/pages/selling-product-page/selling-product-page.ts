import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';


@IonicPage()
@Component({
    selector: 'selling-product-page',
    templateUrl: 'selling-product-page.html',
})

export class SellingProductPage {
    private consumerData: FormGroup;
    private itemVal: string
    private totalPrice: any;
    private productList: any = [];
    private freebieProductList: any = [];
    private searchProductList: any = [];
    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public extraComponents: ExtraComponents,
        public http: Http) {
        this.consumerData = this.formBuilder.group({
            name: ['', Validators.required],
        });
        this.totalPrice = 0;

        console.log(this.navParams.get('consumerId'));
        
    }

    ionViewDidLoad() {
        this.extraComponents.showLoading();
        this.getInventoryDetails().toPromise()
            .then(result => {
                this.extraComponents.closeLoading();
                this.productList = result.json();
                this.productList.forEach(element => {
                    element.count = 0;
                    element.totalPrice = 0;
                });
                this.searchProductList = this.productList;
                this.freebieProductList = JSON.parse(JSON.stringify(this.productList));
                console.log(this.productList);

            }).catch(error => {
                this.extraComponents.closeLoading();
                console.log("Error while fetching inventory list" + error);
            });
    }

    getInventoryDetails() {
        let id = JSON.parse(localStorage.getItem('distributor')).id;

        let param: any = '?id=' + id;
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.inventoryDetails + param);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.inventoryDetails + param, options);
    }


    subtract(product: any) {

        if (product.count > 0)
            product.count--;
        this.totalPrice -= product.totalPrice;
        product.totalPrice = product.productId.price * product.count;
        this.totalPrice += product.totalPrice;
    }

    add(product: any) {

        if (product.count < (product.totalStock - product.soldStock))
            product.count++;
        this.totalPrice -= product.totalPrice;
        product.totalPrice = product.productId.price * product.count;
        this.totalPrice += product.totalPrice;
    }

    submit() {
        this.navCtrl.push('OrderDetailPage', { productList: this.searchProductList, totalPrice: this.totalPrice, buyer: this.navParams.get('consumerId') })
    }

    openSchemePage(){
        this.navCtrl.push('SchemBasedProductSelectionPage', { productList: this.searchProductList, freebieProductList: this.freebieProductList,  totalPrice: this.totalPrice, buyer: this.navParams.get('consumerId') });
    }
    getItems(ev) {
        // set val to the value of the ev target
        var val = ev.target.value;

        console.log('product search');
        this.productList = this.searchProductList;
        this.productList = this.productList.filter((item) => {
            return (item.productId.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
        });
    }
}

