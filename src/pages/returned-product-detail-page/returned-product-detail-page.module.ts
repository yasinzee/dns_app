import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ReturnedProductDetailPage} from './returned-product-detail-page';

@NgModule({
  declarations: [
    ReturnedProductDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ReturnedProductDetailPage),
  ],
  exports: [
    ReturnedProductDetailPage
  ]
})
export class ReturnedProductDetailPageModule {}