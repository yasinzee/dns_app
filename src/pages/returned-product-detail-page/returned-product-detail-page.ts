import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ExtraComponents } from '../../providers/extra-components';
import *  as AppConfig from '../../app/config';

@IonicPage()
@Component({
    selector: 'returned-product-detail-page',
    templateUrl: 'returned-product-detail-page.html'
})
export class ReturnedProductDetailPage {
    private returnedProductList: any = [];
    private consumerName:any;
    private totalAmount:any;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,
        public http: Http) {

        this.navParams.get('orderDetail');
        this.consumerName = this.navParams.get('consumerName');
        this.totalAmount=0;
    }
    ionViewDidLoad() {
        this.fetchReturnedProducts().toPromise()
            .then(result => {
                console.log(result.json());
                this.returnedProductList = result.json();
                if(null!=this.returnedProductList){
                    this.returnedProductList.forEach(element => {
                        this.totalAmount += element.amount;
                    });
                }
                
            }).catch(error => {
                console.log("Error while fetching inventory list" + error);
            });
    }

    fetchReturnedProducts() {
        console.log("params");

        let param: any = '?id=' + this.navParams.get('orderId');;
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchReturnedProducts + param);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchReturnedProducts + param, options);
    }
}
