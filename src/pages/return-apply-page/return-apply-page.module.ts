import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ReturnApplyPage} from './return-apply-page';

@NgModule({
  declarations: [
    ReturnApplyPage,
  ],
  imports: [
    IonicPageModule.forChild(ReturnApplyPage),
  ],
  exports: [
    ReturnApplyPage
  ]
})
export class ReturnApplyPageModule {}