import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';
import { HttpClient } from '../../providers/http-client';

@IonicPage()
@Component({
    selector: 'return-apply-page',
    templateUrl: 'return-apply-page.html',
})

export class ReturnApplyPage {
    private consumerData: FormGroup;
    private itemVal: string
    private totalPrice: any;
    private productList: any = [];
    private freebieProductList: any = [];
    private amount: any = [];
    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public extraComponents: ExtraComponents,
        public httpClient: HttpClient) {

        this.totalPrice = 0;
    }

    ionViewDidLoad() {
        this.extraComponents.showLoading();
        this.fetchAllOrderedProducts().toPromise()
            .then(result => {
                this.extraComponents.closeLoading();
                console.log(result.json());
                this.productList = result.json().orderedProductList;
                this.productList.forEach(element => {
                    element.count = element.quantity;
                    element.totalPrice = element.soldAmount;
                    this.totalPrice += element.soldAmount;
                });
                this.freebieProductList = result.json().discountedProductsList;
                this.freebieProductList.forEach(element => {
                    element.count = element.quantity;
                    element.totalPrice = element.soldAmount;
                    this.totalPrice += element.soldAmount;
                });
            }).catch(error => {
                this.extraComponents.closeLoading();
                console.log("Error while fetching inventory list" + error);
            });
    }

    fetchAllOrderedProducts() {
        let id = this.navParams.get('orderId');
        let param: any = '?id=' + id;
    
        return this.httpClient.get(AppConfig.cfg.fetchOrderedProducts + param);
      }

    subtract(product: any) {
        if (product.count > 0)
            product.count--;
        this.totalPrice -= product.totalPrice;
        
        product.totalPrice = (product.soldAmount/product.quantity) * product.count;
        this.totalPrice += product.totalPrice;
    }

    add(product: any) {
        if (product.count < product.quantity)
            product.count++;
        this.totalPrice -= product.totalPrice;
        product.totalPrice = (product.soldAmount/product.quantity) * product.count;
        this.totalPrice += product.totalPrice;

    }

    submit() {
        this.navCtrl.push('ReturnOrderConfirmPage', { productList: this.productList, freebieProductList: this.freebieProductList, totalPrice: this.totalPrice, orderId: this.navParams.get('orderId') })
    }
}

