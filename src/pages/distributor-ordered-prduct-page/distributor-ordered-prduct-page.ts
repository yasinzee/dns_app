import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';

@IonicPage()
@Component({
    selector: 'distributor-ordered-prduct-page',
    templateUrl: 'distributor-ordered-prduct-page.html'
})
export class OrderedProductsDetailPage {
    private order: any;
    private productList:any=[];
    private nonDiscountedProductList:any=[];
    private discountedProductList:any=[];
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,
        public http: Http) {

        menuCtrl.enable(true);
        this.navCtrl.swipeBackEnabled = false;
        this.order = this.navParams.get('orderDetail');
    }

    ionViewDidLoad() {
        this.fetchAllOrders().toPromise()
            .then(result => {
                console.log(result.json());
                this.productList = result.json();
                if(null!=this.productList){
                    this.nonDiscountedProductList = this.productList.orderedProductList;
                    this.discountedProductList = this.productList.discountedProductsList;
                }
            }).catch(error => {
                console.log("Error while fetching inventory list" + error);
            });
    }

    fetchAllOrders() {
        console.log("params");

        let param: any = '?id=' + this.order.id;
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchOrderedProducts + param);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchOrderedProducts + param, options);
    }

}
