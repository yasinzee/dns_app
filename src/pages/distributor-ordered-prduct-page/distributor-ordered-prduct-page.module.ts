import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {OrderedProductsDetailPage} from './distributor-ordered-prduct-page';

@NgModule({
  declarations: [
    OrderedProductsDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(OrderedProductsDetailPage),
  ],
  exports: [
    OrderedProductsDetailPage
  ]
})
export class OrderedProductsDetailPageModule {}