import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController, ) {

    menuCtrl.enable(true);
    this.navCtrl.swipeBackEnabled = false;
  }

  openPage(page: string) {
    console.log("clicked");    
    this.navCtrl.push(page);
  }
}
