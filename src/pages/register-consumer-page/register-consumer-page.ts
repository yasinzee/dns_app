import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController , NavParams} from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';

@IonicPage()
@Component({
    selector: 'register-consumer-page',
    templateUrl: 'register-consumer-page.html',
})

export class RegisterConsumerPage {
    private consumerData: FormGroup;
    private consumerId:any;
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public formBuilder: FormBuilder,
        public extraComponents: ExtraComponents,
        public http: Http) {
        this.consumerData = this.formBuilder.group({
            name: [null, Validators.compose([Validators.required])],
            email: [null, Validators.compose([Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],
            mobile: [null, Validators.compose([Validators.required, Validators.pattern("[0-9]{10}")])],
            state: [null, Validators.required],
            city: [null, Validators.required],
            address: ['', Validators.required],
        });
        this.consumerData.controls['city'].setValue(JSON.parse(localStorage.getItem('distributor')).city);
        this.consumerData.controls['state'].setValue(JSON.parse(localStorage.getItem('distributor')).state);
    }


    ionViewDidLoad() {
        if(this.navParams.get('consumerData')){
            let consumerDetail  = this.navParams.get('consumerData');
            this.consumerData.controls['name'].setValue(consumerDetail.name);
            this.consumerData.controls['email'].setValue(consumerDetail.email);
            this.consumerData.controls['mobile'].setValue(consumerDetail.mobile);
            this.consumerData.controls['address'].setValue(consumerDetail.address);
            this.consumerId = consumerDetail.id;
        }
    }


    register(formData: any) {

        this.storeConsumer(formData).toPromise()
            .then(result => {
                if(result.status==208){
                    console.log("Member already exist");
                    console.log(result);
                    this.extraComponents.showAlert("Error", new String("Member already exist"));
                }
                else if(result.status==400){
                    console.log("Error");
                    console.log(result);
                    this.extraComponents.showAlert("Error", new String("Internal Server Error. Plz Try Again!!"));
                }
                else{
                    console.log("Stored Successfully");
                    console.log(result);
                    this.extraComponents.showAlert("Success", new String("Consumer Saved Successfully"));
                    if(this.navParams.get('consumerData')){
                        this.navCtrl.push('UpdateDetailPage');    
                    }
                    this.navCtrl.push('SellingProductPage');
                }
                
            }).catch(error => {
                console.log(error);
                this.extraComponents.showAlert("Error Occured. Plz Try Again!!", error);
            });
    }

    storeConsumer(formData: any) {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });

        if(this.navParams.get('consumerData'))
            formData.id = this.consumerId;
        console.log("Form Data");
        console.log(formData);
        console.log("url: " + AppConfig.cfg.apiUrl + AppConfig.cfg.addConsumer);
        return this.http.post(AppConfig.cfg.apiUrl + AppConfig.cfg.addConsumer, formData, options);
    }
}

