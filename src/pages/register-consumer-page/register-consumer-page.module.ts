import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {RegisterConsumerPage} from './register-consumer-page';

@NgModule({
  declarations: [
    RegisterConsumerPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterConsumerPage),
  ],
  exports: [
    RegisterConsumerPage
  ]
})
export class RegisterConsumerPageModule {}