import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';
import { HttpClient } from '../../providers/http-client';

@IonicPage()
@Component({
  selector: 'distributor-orders-page',
  templateUrl: 'distributor-orders-page.html'
})
export class DistributorOrdersPage {
  private paidOrderList: any = [];
  private duesOrderList: any = [];
  private info: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public extraComponents: ExtraComponents,
    public http: Http,
    public alertCtrl: AlertController,
    public httpClient: HttpClient) {

    menuCtrl.enable(true);
    this.navCtrl.swipeBackEnabled = false;
    this.info = 'paid';
  }

  ionViewDidLoad() {
    this.extraComponents.showLoading();
    this.fetchAllPaidOrders().toPromise()
      .then(result => {
        this.extraComponents.closeLoading();
        console.log(result.json());
        this.paidOrderList = result.json();
      }).catch(error => {
        this.extraComponents.closeLoading();
        console.log("Error while fetching inventory list" + error);
      });

    this.fetchAllDuesOrders().toPromise()
      .then(result => {
        console.log(result.json());
        this.duesOrderList = result.json();
      }).catch(error => {
        console.log("Error while fetching inventory list" + error);
      });

  }

  fetchAllPaidOrders() {
    console.log("params");
    let id = JSON.parse(localStorage.getItem('distributor')).id;

    let param: any = '?id=' + id;
    // let headers = new Headers();
    // headers.append("Accept", "application/json");
    // headers.append('Content-Type', 'application/json');
    // let options = new RequestOptions({ headers: headers });
    // console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchPaidOrders + param);

    // return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchPaidOrders + param, options);

    return this.httpClient.get(AppConfig.cfg.fetchPaidOrders + param);
  }

  fetchAllDuesOrders() {
    console.log("params");
    let id = JSON.parse(localStorage.getItem('distributor')).id;

    let param: any = '?id=' + id;
    // let headers = new Headers();
    // headers.append("Accept", "application/json");
    // headers.append('Content-Type', 'application/json');
    // let options = new RequestOptions({ headers: headers });
    // console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchDuesOrders + param);

    // return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchDuesOrders + param, options);

    return this.httpClient.get(AppConfig.cfg.fetchDuesOrders + param);
  }

  redirect(order: any) {
    console.log("clicked for details");
    console.log(order);
    this.navCtrl.push('OrderedProductsDetailPage', { orderDetail: order });
  }

  viewReturnedProductPage(order: any) {
    this.navCtrl.push('ReturnedProductDetailPage', { orderId: order.id, consumerName: order.consumer.name });
  }

  viewReturnApplyPage(order: any) {
    this.navCtrl.push('ReturnApplyPage', { orderId: order.id});
  }

  ShowSettleUpPrompt(order: any) {
    const prompt = this.alertCtrl.create({
      title: 'Pay Dues Amount',
      inputs: [
        {
          name: 'amount',
          placeholder: 'Enter Amount',
          type:'number'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Settle Up',
          handler: data => {
            console.log(order);
            console.log(data);
            this.settleDuesAmount(order, data);
          }
        }
      ]
    });
    prompt.present();
  }

  settleDuesAmount(order:any, data:any){
    if(order.amountDues>=data.amount){
      this.extraComponents.showLoading();
      console.log(AppConfig.cfg.settleDuesAmount);
      
      this.httpClient.post(AppConfig.cfg.settleDuesAmount+"/"+order.id+"/"+data.amount, "").toPromise()
      .then(result => {
        this.extraComponents.closeLoading();
        this.extraComponents.showToast('Dues Amount Updated Successfully');
        console.log(result.json());
      }).catch(error => {
        this.extraComponents.closeLoading();
        this.extraComponents.showToast('Error Occured, Plz Try Again');
      });
      
    }
    else{
      this.extraComponents.showToast('Please enter a valid amount');
    }
  }
}
