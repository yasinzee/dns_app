import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DistributorOrdersPage} from './distributor-orders-page';

@NgModule({
  declarations: [
    DistributorOrdersPage,
  ],
  imports: [
    IonicPageModule.forChild(DistributorOrdersPage),
  ],
  exports: [
    DistributorOrdersPage
  ]
})
export class DistributorOrdersPageModule {}