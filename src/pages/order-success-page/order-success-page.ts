import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { ExtraComponents } from '../../providers/extra-components';

@IonicPage()
@Component({
  selector: 'order-success-page',
  templateUrl: 'order-success-page.html'
})
export class OrderSuccessPage {
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public extraComponents: ExtraComponents, ) {

    menuCtrl.enable(true);
    this.navCtrl.swipeBackEnabled = false;
  }
  openOrdersPage(){
    this.navCtrl.push('DistributorOrdersPage');
  }
  openHomePage(){
    this.navCtrl.setRoot('HomePage');
  }
}
