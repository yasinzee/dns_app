export let cfg = {
    // apiUrl: 'http://dnstest2.ap-south-1.elasticbeanstalk.com/',
    // apiUrl: 'http://dnsadmin.ap-south-1.elasticbeanstalk.com/',
    apiUrl: 'http://localhost:8080/',
    addDistributor: 'distributor/add',
    changePasswork: 'distributor/change/password',
    addProduct: 'product/add',
    authenticate:'login/authenticate',
    inventoryDetails: 'inventory/fetch',
    saveOrder: 'distributor/order',

    fetchDuesOrders: 'distributor/orders/fetch/dues',
    fetchPaidOrders: 'distributor/orders/fetch/paid',
    fetchReturnedProducts: 'order/return/fetch',
    fetchOrderedProducts: 'distributor/orders/product',
    applyReturn: 'order/return',

    addConsumer: 'consumer/add',
    fetchConsumerList: 'consumer/fetch',

    settleDuesAmount: 'distributor/orders/settle/dues/',
    fetchConsumerTotalDuesAmount: 'distributor/orders/duesamount',
}