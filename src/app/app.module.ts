import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ExtraComponents } from '../providers/extra-components';
import { MyApp } from './app.component';
import { HttpClient } from '../providers/http-client';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import {ConsumerDetailModal} from '../pages/consumer-detail-modal/consumer-detail-modal';

@NgModule({
  declarations: [
    MyApp,ConsumerDetailModal
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,ConsumerDetailModal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ExtraComponents,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HttpClient
  ]
})
export class AppModule {}
