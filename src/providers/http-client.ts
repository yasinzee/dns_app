import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../app/config';

@Injectable()
export class HttpClient {
    constructor(public http: Http) {}

    post(url: any, body:any) {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });

        console.log("body");
        console.log(body);
        console.log("url: " + AppConfig.cfg.apiUrl + url);
        return this.http.post(AppConfig.cfg.apiUrl + url, body, options);
    }

    get(url: any){
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + url);
    
        return this.http.get(AppConfig.cfg.apiUrl + url, options);
    }
}