import { LoadingController, AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ExtraComponents {
  private loader: any;
  private alert: any;
  constructor(public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController, ) { }

  showAlert(alertTitle: string, message: object) {
    this.alert = this.alertCtrl.create({
      title: alertTitle,
      subTitle: message.toString(),
      buttons: ['OK']
    });
    this.alert.present();
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      content: `<div style="background-color:transparent;">
      Please wait...</div>`,
      spinner: 'bubbles',
      cssClass: 'my-loading-class',
    });
    this.loader.present();
  }

  closeLoading() {
    this.loader.dismiss();
  }
  
  showToast(message: any) {
    let toast = this.toastCtrl.create({
      message: message,
      position: 'bottom',
      duration: 3000,
      cssClass: 'yourtoastclass'
    });
    toast.present();
  }
}